package prueba.veritran.net.pruebauniversidad;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UniversityUnitTest {
    @Test
    public void ceroTest() {
        assertEquals( "quinientos ochenta", MontoEscrito.getMontoEscrito(580));
    }

    @Test
    public void nueveTest() {
        assertEquals( "diez y nueve",MontoEscrito.getMontoEscrito(19));
    }

    @Test
    public void milTest() {
        assertEquals("mil", String.join("",MontoEscrito.getMontoEscrito(1000)));
    }

    @Test
    public void mil_1_Test() {
        assertEquals("nueve mil ciento cincuenta y seis", MontoEscrito.getMontoEscrito(9156));
    }

    @Test
    public void millonTest() {
        assertEquals("un millon",  MontoEscrito.getMontoEscrito(1000000));
    }

    @Test
    public void millon_1_Test() {
        assertEquals( "tres millones doscientos noventa mil seiscientos cuarenta y uno", MontoEscrito.getMontoEscrito(3290641));
    }

    @Test
    public void millon_1_Ts() {
        assertEquals( "cien millones",MontoEscrito.getMontoEscrito(100000000));
    }






}