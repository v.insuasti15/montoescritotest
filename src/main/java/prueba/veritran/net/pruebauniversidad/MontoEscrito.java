package prueba.veritran.net.pruebauniversidad;
//valeri insuasti
import java.util.HashMap;
import java.util.Map;

public class MontoEscrito {

    private static Map<Integer,String> parseoNumeros = new HashMap<Integer,String>();

    public static void main(String[] args) {

        MontoEscrito.getMontoEscrito(0);
    }

    public static String getMontoEscrito(Integer valor){

        crearParseNumeros();

        String resultado = "";

      /*XXX XXX XXX*/
        if ((valor/1000000) > 0){ //1 000 000 a infinito

            if((valor/1000000) == 1){//1 000 000 o 1 999 999
                resultado =(parseoNumeros.containsKey(valor))?parseoNumeros.get(valor): parseoNumeros.get(1000000) + " " +/*100 000 a 999 999*/ getMontoEscrito(valor%1000000);
            }else{
                // X 000 000 o X 99 999 999
                resultado =((valor%100000) == 0)? getMontoEscrito(valor/1000000) + " millones" : getMontoEscrito(valor/1000000) + " millones " + getMontoEscrito(valor%1000000);
            }
         // XXX XXX
        }else if((valor/1000) > 0){  //1 000 a 999 999

            if ((valor/1000) == 1){
                // 1 000 o 1001 a 1 999
                resultado = (parseoNumeros.containsKey(valor))? parseoNumeros.get(valor): parseoNumeros.get(1000) + " " + getMontoEscrito(valor%1000);
            }else{
               // 2 000 a XXX 000  o 2 999 a XXX 999
                resultado =((valor%1000) == 0)? getMontoEscrito(valor/1000) + " " + parseoNumeros.get(1000): getMontoEscrito(valor/1000) + " " + parseoNumeros.get(1000) + " " + getMontoEscrito(valor%1000);

            }
            // XXX
        } else if ((valor/100) > 0){ //100 a 999
            if ((valor/100) == 1){
                // 100 o 101 a 999
                resultado =(parseoNumeros.containsKey(valor))? parseoNumeros.get(valor): "ciento " + getMontoEscrito(valor%100);

            }else if((valor/100) == 5 || (valor/100) == 7 || (valor/100) == 9) { // 5XX 7XX 9XX

                resultado = ((valor%100) == 0)? parseoNumeros.get(100*(valor/100)):parseoNumeros.get(100*(valor/100)) + " " + getMontoEscrito(valor%100);

            }else {
                // 200 a X00  o 299 a X99
                resultado = ((valor%100) == 0)? parseoNumeros.get(valor/100) + "cientos ": parseoNumeros.get(valor/100) + "cientos " + getMontoEscrito(valor%100);

            }
            // XX
        }else if ((valor/10) > 0){ //0 a 99

                if (valor/10==1 || valor/10==2  ) { //10 a 29
                    resultado = parseoNumeros.get(valor);
                }

                else { //30 a 99
                    resultado =(parseoNumeros.containsKey(valor)) ? parseoNumeros.get(valor): parseoNumeros.get( 10*(valor / 10)) + " y " + getMontoEscrito((valor % 10));
                }

        }else{

            if (parseoNumeros.containsKey(valor)){
                resultado = parseoNumeros.get(valor);
            }
        }

        return resultado;
    }

    private static void crearParseNumeros() {

        parseoNumeros.put(0,"cero");
        parseoNumeros.put(1,"uno");
        parseoNumeros.put(2,"dos");
        parseoNumeros.put(3,"tres");
        parseoNumeros.put(4,"cuatro");
        parseoNumeros.put(5,"cinco");
        parseoNumeros.put(6,"seis");
        parseoNumeros.put(7,"siete");
        parseoNumeros.put(8,"ocho");
        parseoNumeros.put(9,"nueve");
        parseoNumeros.put(10, "diez");
        parseoNumeros.put(11, "once");
        parseoNumeros.put(12, "doce");
        parseoNumeros.put(13, "trece");
        parseoNumeros.put(14, "catorce");
        parseoNumeros.put(15, "quince");
        parseoNumeros.put(16, "Diez y seis");
        parseoNumeros.put(17, "Diez y siete");
        parseoNumeros.put(18, "Diez y ocho");
        parseoNumeros.put(19, "diez y nueve");
        parseoNumeros.put(20, "veinte");
        parseoNumeros.put(21, "veintiuno");
        parseoNumeros.put(22, "veintidos");
        parseoNumeros.put(23, "veintitres");
        parseoNumeros.put(24, "veinticuatro");
        parseoNumeros.put(25, "veinticinco");
        parseoNumeros.put(26, "veintiseis");
        parseoNumeros.put(27, "veintisiete");
        parseoNumeros.put(28, "veintiocho");
        parseoNumeros.put(29, "veintinueve");
        parseoNumeros.put(30, "treinta");
        parseoNumeros.put(40, "cuarenta");
        parseoNumeros.put(50, "cincuenta");
        parseoNumeros.put(60, "sesenta");
        parseoNumeros.put(70, "setenta");
        parseoNumeros.put(80, "ochenta");
        parseoNumeros.put(90, "noventa");
        parseoNumeros.put(100, "cien");
        parseoNumeros.put(500, "quinientos");
        parseoNumeros.put(700, "setecientos");
        parseoNumeros.put(900, "novecientos");
        parseoNumeros.put(1000, "mil");
        parseoNumeros.put(1000000, "un millon");
    }
}
